// Slide page : Home After - Best Recommendation
new Splide('#slide-home-after', {
    perPage: 4,
    pagination: false,
    rewind: true,
}).mount();

// Slide Page  : Home After - Most Popular
new Splide('#slide-most-popular', {
    perPage: 4,
    pagination: false,
    rewind: true,
}).mount();